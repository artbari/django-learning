.. _activity_views:


Activity views
--------------

.. automodule:: learning.views.activity
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:
