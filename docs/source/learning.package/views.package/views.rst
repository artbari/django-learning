.. _views:

views
=====

Views that match a model
------------------------
.. toctree::
   :maxdepth: 4

   resource
   activity
   course

Tools
-----
.. toctree::
   :maxdepth: 4

   helpers

Other views
-----------

.. automodule:: learning.views.views
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

