API Documentation
=================

This chapter covers the documentation of the ``django-learning`` API. The mains parts you should focus on are:

* **models**: the models used in the application, Course, Activity, Resource
* **exception**: the exceptions raised by models
* **views**:

  * **course views**: views used to manipulate **Course** objects (CRUD), link and unlink activities, as well as viewing nested activities and resources.
  * **activity views**: views used to manipulate **Activity** objects (CRUD), link and unlink resources, as well as viewing nested resources.
  * **resource views**: views used to manipulate **Resource** objects (CRUD).

.. warning::
   This documentation is updated against the project **master** branch only. If you wish to get the documentation for the current developments, you’ve got to build it by yourself.

.. toctree::
   :maxdepth: 8

   learning.package/learning
   learning.package/views.package/views

